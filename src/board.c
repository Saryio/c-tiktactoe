#include <tiktactoe/board.h>
#include <stdio.h>

static ui8 getIndex(ui8 line, ui8 column){
    return (3 * (line - 1) + (column - 1));
}

static char getChar(Piece piece){
    return ((piece == None) ? ' ' : ((piece == X) ? 'X' : 'O'));
}

static Piece overOnLine(Board board){
    for(ui8 i=1; i<=BOARD_LINE; i++){
        Piece piece = board[getIndex(i, 1)];
        if(piece != None && board[getIndex(i, 2)] == piece && board[getIndex(i, 3)] == piece)
            return piece;
    }
    return None;
}

static Piece overOnColumn(Board board){
    for(ui8 i=1; i<=BOARD_COLUMN; i++){
        Piece piece = board[getIndex(1, i)];
        if(piece != None && board[getIndex(2, i)] == piece && board[getIndex(3, i)] == piece)
            return piece;
    }
    return None;
}

static Piece overOnDiagonal(Board board){
    Piece piece;

    piece = board[getIndex(1, 1)];
    if(piece != None && board[getIndex(2, 2)] == piece && board[getIndex(3, 3)] == piece){
        return piece;
    }

    piece = board[getIndex(1,3)];
    if(piece != None && board[getIndex(2, 2)] == piece && board[getIndex(3, 1)] == piece)
        return piece;
    
    return None;
}



void Board_clean(Board board){
    for(ui8 i=0; i < BOARD_SIZE; i++){
        board[i] = None;
    }
}

void Board_set(Board board, ui8 line, ui8 column, Piece piece){
    board[getIndex(line, column)] = piece;
}

Piece Board_get(Board board, ui8 line, ui8 column){
    return board[getIndex(line, column)];
}

void Board_draw(Board board){
    printf(" %c | %c | %c\n"
           "-----------\n"
           " %c | %c | %c\n"
           "-----------\n"
           " %c | %c | %c\n",
           getChar(board[0]), getChar(board[1]), getChar(board[2]),
           getChar(board[3]), getChar(board[4]), getChar(board[5]),
           getChar(board[6]), getChar(board[7]), getChar(board[8]));
}

Piece Board_isOver(Board board){
    Piece piece;
    
    if((piece = overOnLine(board)) != None) return piece;
    if((piece = overOnColumn(board)) != None) return piece;
    if((piece = overOnDiagonal(board)) != None) return piece;

    return None;
}
