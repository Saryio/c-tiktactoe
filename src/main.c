#include <tiktactoe/game.h>

ui8 main(){
    Game game;
    Game_CONSTRUCT(&game);

    return Game_start(&game);
}
