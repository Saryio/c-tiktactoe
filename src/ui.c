#include <tiktactoe/ui/ui.h>
#include <stdlib.h>

UI* UI_NEW(){
    return malloc(sizeof(UI));
}

void UI_DELETE(UI* ui){
    free(ui);
}

void UI_CONSTRUCT(UI* ui){
    for(ui8 i=0; i<UI_MAX_SCREENS_COUNT; i++) ui->screens[i] = NULL;
    ui->currentScreen = NULL;
    ui->screenCount = 0;
}

void UI_update(UI *ui){
    if(ui->currentScreen) Screen_update(ui->currentScreen);
}

void UI_draw(UI *ui){
    if(ui->currentScreen) Screen_draw(ui->currentScreen);
}