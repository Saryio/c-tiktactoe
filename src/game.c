#include <tiktactoe/game.h>
#include <tiktactoe/keyboard.h>
#include <tiktactoe/ui/ui.h>
#include <stdlib.h>

Game* Game_NEW(){
    return malloc(sizeof(Game));
}

void Game_DELETE(Game* game){
    free(game);
}

void Game_CONSTRUCT(Game* game){
    UI_CONSTRUCT(&(game->ui));
}

ui8 Game_start(Game *game){
    Game_loop(game);
    return 0;
}

void Game_loop(Game *game){
    while(true){
        // Keyboard_update();
        UI_update(&(game->ui));

        UI_draw(&(game->ui));
    }
}
