#include <tiktactoe/ui/screen.h>

void Screen_update(Screen *screen){
    screen->update();
}

void Screen_draw(Screen *screen){
    screen->draw();
}