cc = gcc

includes = -I include

co = $(cc) $(includes) -c -o $@ $^

all: ttt

ttt: main.o game.o board.o ui.o screen.o
	$(cc) $(includes) -o $@ $^

main.o: src/main.c
	$(co)

game.o: src/game.c
	$(co)

board.o: src/board.c
	$(co)

ui.o: src/ui.c
	$(co)

screen.o: src/screen.c
	$(co)

run: ttt
	clear
	./ttt

clean:
	rm ttt *.o

rulean: clear run clean
clear:
	clear
