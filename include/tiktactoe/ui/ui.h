#ifndef __TIK_TAC_TOE_UI_UI_H__
#define __TIK_TAC_TOE_UI_UI_H__

#include <utils/types.h>
#include <tiktactoe/ui/screen.h>

#define UI_MAX_SCREENS_COUNT 16

typedef struct { 
    Screen *screens[UI_MAX_SCREENS_COUNT];
    Screen *currentScreen;
    ui8 screenCount;
} UI;


UI* UI_NEW();
void UI_DELETE(UI* ui);
void UI_CONSTRUCT(UI* ui);

void UI_update(UI *ui);
void UI_draw(UI *ui);
void UI_addScreen(UI *ui);
// void UI_

#endif // __TIK_TAC_TOE_UI_UI_H__

/*
    - Welcome screen
    - Ask nickname screen
    - Menu screen
        - Start Game
            - Multiplayer (PvP)
            - Player vs A.I.
        - Config
            - Change terminal color
    - In game screen
    - Game over screen
*/
