#ifndef __TIK_TAC_TOE_UI_SCREEN_H__
#define __TIK_TAC_TOE_UI_SCREEN_H__

#include <utils/types.h>

typedef struct {
    void (*update)();
    void (*draw)();
} Screen;

void Screen_update(Screen *screen);
void Screen_draw(Screen *screen);

#endif // __TIK_TAC_TOE_UI_SCREEN_H__