#ifndef __TIK_TAC_TOE_BOARD_H__
#define __TIK_TAC_TOE_BOARD_H__

#include <utils/types.h>

#define BOARD_LINE 3
#define BOARD_COLUMN 3
#define BOARD_SIZE (BOARD_LINE*BOARD_COLUMN)

typedef enum {
    None = 0,
    X,
    O
} Piece;

typedef Piece Board[BOARD_SIZE];

void Board_clean(Board board);
void Board_set(Board board, ui8 line, ui8 column, Piece piece);
Piece Board_get(Board board, ui8 line, ui8 column);
void Board_draw(Board board);
Piece Board_isOver(Board board);

#endif // __TIK_TAC_TOE_BOARD_H__