#ifndef __TIK_TAC_TOE_GAME_H__
#define __TIK_TAC_TOE_GAME_H__

#include <utils/types.h>
#include <tiktactoe/ui/ui.h>

typedef struct{
    UI ui;
} Game;


ui8 Game_start(Game *game);
void Game_loop(Game *game);

#endif // __TIK_TAC_TOE_GAME_H__