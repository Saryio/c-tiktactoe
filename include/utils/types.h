#ifndef __UTILS_TYPES_H__
#define __UTILS_TYPES_H__

#include <stdint.h>

// Int's
typedef uint8_t ui8;
typedef uint16_t ui16;
typedef uint32_t ui32;
typedef uint64_t ui64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;


// Float's
typedef float f32;
typedef double f64;


// Char's
typedef unsigned char uchar;
// typedef char char;


// Boolean's
typedef uint8_t bool;
#define false 0
#define true 1

#endif // __UTILS_TYPES_H__
